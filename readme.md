# Node Red

```bash
    docker-compose up -d 
    sudo chmod -R 777 add-nr-modules.sh 
    ./add-nr-modules.sh 
```

## Exemplo de execução

```bash
docker run -it -p 5987:1880 -v /tmp/nodered:/data --name nodered-local nodered/node-red
docker run -it -p 5987:1880 -d --name nodered-local nodered/node-red
docker run -it -p 1880:1880 -v node_red_data:/data --name mynodered nodered/node-red
```

## Install Manege Palette

- node-red-contrib-crypt
- node-red-contrib-uibuilder
- node-red-node-mysql
- node-red-node-serialport
- node-red-node-sqlite

## Link documentação

- https://nodered.org/docs/getting-started/docker
- https://github.com/node-red/node-red-docker  
- https://docs.teslamate.org/docs/integrations/node-red/