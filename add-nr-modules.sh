#!/bin/bash
:
MODULES="node-red-contrib-calc
node-red-contrib-simpletime
node-red-dashboard
node-red-node-email
node-red-contrib-telegrambot
node-red-node-ui-table
node-red-contrib-crypt
node-red-contrib-uibuilder
node-red-node-mysql
node-red-node-serialport
node-red-node-sqlite
node-red-laravel-websockets"
set -x
for MODULE in $MODULES; do
    docker-compose exec -T node-red npm install --no-audit --no-update-notifier --no-fund --save --save-prefix=~ --production $MODULE
done
docker-compose stop node-red
docker-compose start node-red
